/* 
 * Math Operations
 * Designer: Forrest Bushstone
 */
`timescale 1ns / 1ps

module add(input [15:0] ri0, input [15:0] ri1, output reg [15:0] ro);
    reg carry;
    integer i;
    always @ (ri0, ri1, carry, ro) begin
        carry = 0;
        for (i = 0; i < 16; i = i + 1) begin
            ro[i] = (ri0[i] ^ ri1[i]) ^ carry;
            carry = (ri0[i] & ri1[i]) | ((ri0[i] ^ ri1[i]) & carry); 
        end
    end
endmodule

module sub(input [15:0] ri0, input [15:0] ri1, output reg [15:0] ro);
    reg carry;
    integer i;
    always @ (ri0, ri1, carry, ro) begin
        carry = 0;
        for (i = 0; i < 16; i = i + 1) begin
            ro[i] = (ri0[i] ^ ~ri1[i]) ^ carry;
            carry = (ri0[i] & ~ri1[i]) | ((ri0[i] ^ ~ri1[i]) & carry); 
        end
    end
endmodule

module mul(input [15:0] ri0, input [15:0] ri1, output reg [15:0] ro);
    reg [15:0] tmp;
    reg [15:0] tmpro;
    reg carry;
    integer i, j;
    always @ (ri0, ri1, ro) begin
        tmpro = 16'b0;
        tmp = 16'b0;
        carry = 0;
        for (i = 0; i < 16; i = i + 1) begin
            tmp = 16'b0;
            for (j = 0; j < 16; j = j + 1) begin
                tmp = tmp | (((ri0[j] & ri1[i]) ? 1 : 0) << i << j);
            end
            //add(.ri0(tmp), .ri1(tmpro), .ro(tmpro));
            for (j = 0; j < 16; j = j + 1) begin
                tmpro[j] = (tmp[j] ^ tmpro[j]) ^ carry;
                carry = (tmp[j] & tmpro[j]) | ((tmp[j] ^ tmpro[j]) & carry);
            end
        end
        ro = ~tmpro;
    end
endmodule

/* Input 1: Dividend, Input 2: Divisor */
module div(input [15:0] ri0, input [15:0] ri1, output reg [15:0] ro);
    reg [15:0] tmpri0;
    reg [15:0] tmpri0_chk;
    reg [15:0] tmpri1;
    reg [15:0] tmpri0_msb;
    reg [15:0] tmpri1_msb;
    reg [15:0] msbri0;
    reg [15:0] msbri1;
    integer i, j;
    always @ (ri0, ri1, ro) begin
        if (ri1) begin
            tmpri0 = ri0;
            tmpri1 = ri1;
            tmpri0_msb = ri0;
            tmpri1_msb = ri1;
            msbri1 = 0;
            ro = 0;
            for (j = 0; j < 16; j = j + 1) begin
                msbri1 = msbri1 + (tmpri1_msb ? 1 : 0);
                tmpri1_msb = tmpri1_msb >> 1;
            end
            
            for (i = 15; i >= 0; i = i - 1) begin
                tmpri1 = ri1;
                tmpri0_msb = tmpri0;
                tmpri1_msb = ri1;
                msbri0 = 0;
                for (j = 0; j < 16; j = j + 1) begin
                    msbri0 = msbri0 + (tmpri0_msb ? 1 : 0);
                    tmpri0_msb = tmpri0_msb >> 1;
                end
                
                tmpri0_chk = tmpri0 - (tmpri1 << i);
                if (i <= msbri0 - msbri1) begin
                    if (tmpri0_chk < tmpri0) begin
                        tmpri0 = tmpri0_chk;
                        ro = ro | (1 << i);
                    end
                end
            end
        end
    end
endmodule
